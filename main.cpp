#include <iostream>
#include <vector>
//#include <stdlib.h>
//#include <time.h>
using namespace std;

//white - 1
//black - 2
class Neuron {
public:
    int white[11];
    int black[11];


    float weights[11];

    Neuron() {
        srand(time(NULL));
    }

    float summator(int i) {
        return weights[i];
    }

    int activation(int i) {
        if ((rand() % 100) < summator(i) * 100)
            return 1;
        else
            return 2;
    }

    float getWeight(int i) {
        return (float) white[i] / (black[i] + white[i]);

    }

    void updateWeights() {
        for (int i = 0; i < 11; ++i) {
            weights[i] = getWeight(i);
        }
    }

    void init() {
        for (int i = 0; i < 11; ++i) {
            white[i] = 1;
            black[i] = 1;
            weights[i] = getWeight(i);
        }
        black[0] = 0;
        white[1] = 0;
    }
};

class ANN {

    struct action {
        int input;
        int output;

        action(int i, int o) : input(i), output(o) { }
    };

    Neuron neuron;
    vector<action> history;
public:
    ANN() {
        neuron.init();
    }

    int calc(int num) {
        int res = neuron.activation(num - 1);
        history.emplace_back(num - 1, res);
        cout << endl << "i see: " << num << " sticks, i remove: " << res << endl;
        return res;
    }

    void teach(bool win) {
        if (win) {
            for (int i = 0; i < history.size(); ++i) {
                action &currentAction = history[i];
                if (currentAction.output == 1)
                    neuron.white[currentAction.input] += 1;
                else
                    neuron.black[currentAction.input] += 1;
            }
        }
        else {
            action &lastAction = history.back();
            if (lastAction.output == 1)// (means 1 stick removed)
            {
                if (neuron.white[lastAction.input] > 1)
                    neuron.white[lastAction.input] -= 1;
            }
            else if (neuron.black[lastAction.input > 1]) {
                neuron.black[lastAction.input] -= 1;
            }
        }
        neuron.updateWeights();
        history.clear();
    }

};

int playerChoice(int curr) {
    int choice = 0;
    while (choice != 1 && choice != 2) {
        cout << "Your turn. Now: " << curr << " sticks. remove 1 or 2?";
        cin >> choice;

    }

    return choice;
}

int main() {

    ANN network1;
    ANN network2;
    int tries = 1000;
    int firstWin = 0;
    for (int i = 0; i < tries; ++i) {
        int sticks = 11;
        bool firstMove = rand() % 2;
        while (sticks > 0) {
            firstMove = !firstMove;
            if (firstMove)
                cout << "comp1:" << endl;
            else
                cout << "comp2:" << endl;
            sticks -= (firstMove ? network1.calc(sticks) : network2.calc(sticks));

        }
        if (firstMove) {
            cout << endl << "1 win!";
            firstWin++;
        }
        else
            cout << endl << "2 win!";

        network1.teach(firstMove);
        network2.teach(!firstMove);
        //cin.get();
    }
    cout << endl << "First win: " << firstWin << endl;
    cout << "Second win: " << tries - firstWin << endl;


    for (int i = 0; i < 5; ++i) {
        int sticks = 11;
        bool playerMove = rand() % 2;
        while (sticks > 0) {
            playerMove = !playerMove;
            if (playerMove)
                cout << "player:" << endl;
            else
                cout << "comp:" << endl;
            sticks -= (playerMove ? playerChoice(sticks) : network1.calc(sticks));

        }
        if (playerMove)
            cout << endl << "You win!";
        else
            cout << endl << "Comp win!";
        network1.teach(!playerMove);
    }
    return 0;
}